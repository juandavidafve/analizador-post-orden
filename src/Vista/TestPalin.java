/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaS;
import java.util.Scanner;

/**
 *
 * @author DOCENTE
 */
public class TestPalin {

    public static void main(String[] args) {
        ListaS<Character> l = crear();
        System.out.println(l);
        ListaS<Character> l2=l.crearInvertida();
        System.out.println("Crear invertido:"+l2);
        
        if(l.isPalin())
            System.out.println("Si es palíndroma :)");
        else
            System.out.println("NO :( es palíndroma");
        
        ListaS<Character> l3=crear();
        l3.invertir();
        System.out.println("L3 invertida:"+l3);
        
    }

    private static ListaS<Character> crear() {
        System.out.println("Digite una cadena");
        Scanner t = new Scanner(System.in);
        String cadena = t.nextLine();
        ListaS<Character> l = new ListaS();
        for (int i = 0; i < cadena.length(); i++) {
            l.insertarFin(cadena.charAt(i));

        }
        return l;
    }

}
