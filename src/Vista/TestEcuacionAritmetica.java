/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Pila;
import java.util.Scanner;

/**
 *
 * @author juanafanador07
 */
public class TestEcuacionAritmetica {

    public static void main(String[] args) {
        System.out.println("Validando Posfijo");
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();
        //String s = "3,1,2,3,5,3,*,+,/,+,/,1,+";
        String[] a = s.split(",");

        Pila<Double> p = new Pila();

        for (int i = 0; i < a.length; i++) {
            String elemento = a[i];
            if (!esOperacion(elemento)) {
                try {

                    Double valor = Double.parseDouble(elemento);

                    p.push(valor);
                } catch (Exception e) {
                    throw new RuntimeException("Contiene un caracter no valido");
                }
            } else {
                Double num2 = 0.0;
                Double num1 = 0.0;
                Double resultado = 0.0;

                try {
                    num2 = p.pop();
                    num1 = p.pop();
                } catch (Exception e) {
                    throw new RuntimeException("La ecuación está mal parseada");
                }

                switch (elemento.charAt(0)) {
                    case '+':
                        resultado = num1 + num2;
                        break;
                    case '-':
                        resultado = num1 - num2;
                        break;
                    case '*':
                        resultado = num1 * num2;
                        break;

                    case '/':

                        if (num2 == 0) {
                            throw new RuntimeException("No se puede dividir entre 0");
                        }

                        resultado = num1 / num2;
                        break;

                    case '^':
                        resultado = Math.pow(num1, num2);
                        break;
                }

                p.push(resultado);
            }

        }

        System.out.println(p.pop());

    }

    static boolean esOperacion(String s) {

        if (s.length() != 1) {
            return false;
        }

        Character c = s.charAt(0);
        char ch = c.charValue();

        return ch == '+'
                || ch == '-'
                || ch == '*'
                || ch == '/'
                || ch == '^';

    }

}
